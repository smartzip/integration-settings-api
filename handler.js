'use strict';

import {failure, success} from "./libs/response-lib";
import * as dynamoDbLib from "./libs/dynamodb-lib";
const authorizer = require('./authorizer');

const API_ACCESSES_TABLE = `${process.env.DYNAMODB_TABLE_PREFIX}-apiAccessesTable`;


// Used to test valid token
export const hello = async (event, context) => {
  return {
    statusCode: 200,
    body: JSON.stringify({
      // message: `Go Serverless v1.0! ${(await message({ time: 1, copy: 'Your function executed successfully!'}))}`,
      message: `token is valid`,
    }),
  };
};

// const message = ({ time, ...rest }) => new Promise((resolve, reject) =>
//   setTimeout(() => {
//     resolve(`${rest.copy} (with a delay)`);
//   }, time * 1000)
// );

export const generateToken = async (event, context) => {
  // below code creates new record/auth token every time
  // const body = {
  //   "app_name": event.body.app_name,
  //   "access_level": event.body.access_level,
  //   "access_id": event.body.access_id
  // };
  const token = authorizer.generateToken(event.body);
  const data = JSON.parse(event.body);
  

  const params = {
      TableName: API_ACCESSES_TABLE,
      Item: {
          auth_token: token,
          api_key: event.headers["x-api-key"],
          app_name: data.app_name,
          access_level: data.access_level,
          access_id: data.access_id,
          redirect_url: data.redirect_url
      }
  };

  try {
      await dynamoDbLib.call("put", params);
      console.log("--api-accesses insertion successful--");
      return success(params.Item);
  } catch (e) {
        return failure({
          status: false
        });
  }
};

module.exports.authorize = (event, context, callback) => {
  try {
    console.log(event.authorizationToken);
    console.log(event.methodArn);
    
    const policy = authorizer.generatePolicy(event.authorizationToken, event.methodArn);
    callback(null, policy);
  } catch (error) {
    console.log(error.message);
    callback(error.message);
  }
};