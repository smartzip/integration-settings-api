import axios from "axios";
import * as dynamoDbLib from "./libs/dynamodb-lib";
import { success, failure, redirect } from "./libs/response-lib";
const INTEGRATIONS_TABLE = `${process.env.DYNAMODB_TABLE_PREFIX}-integrationsTable`;
const API_ACCESSES_TABLE = `${process.env.DYNAMODB_TABLE_PREFIX}-apiAccessesTable`;
const INTEGRATION_CONNECTIONS_TABLE = `${process.env.DYNAMODB_TABLE_PREFIX}-integrationConnectionsTable`;

export async function callback(event, context) {
  const auth_token = event.queryStringParameters.state.split(':')[0];
  const integration_id = event.queryStringParameters.state.split(':')[1];
  const code = event.queryStringParameters.code;

  var redirect_url = "";
  await new Promise(async function (resolve, reject) {
    const params = {
      TableName: INTEGRATIONS_TABLE,
      Key: {
        integration_id: integration_id
      },
    };

    try {
      const result = await dynamoDbLib.call("get", params);
      if (result.Item) {
        var integrationConnectionItem = await createIntegrationConnection(auth_token, result.Item);
        var response = await getDTRBearerToken(integrationConnectionItem, code, resolve, reject);
      } else {
        return failure({
          status: false,
          error: integration_id + "Integration not found."
        });
      }
    } catch (e) {
      return failure({
        status: false
      });
    }

  }).then(
    async function (success) {
      const api_accesses_params = {
        TableName: API_ACCESSES_TABLE,
        Key: {
          auth_token: auth_token
        },
      };

      try {
        const result = await dynamoDbLib.call("get", api_accesses_params);
        if (result.Item) {
          // redirect to back to the URL
          redirect_url = result.Item.redirect_url;
        } else {
          return failure({
            status: false,
            error: "API accesses for the supplied auth_token not found."
          });
        }
      } catch (e) {
        return failure({
          status: false
        });
      }
    },
    async function (err) {
      const api_accesses_params = {
        TableName: API_ACCESSES_TABLE,
        Key: {
          auth_token: auth_token
        },
      };

      try {
        const result = await dynamoDbLib.call("get", api_accesses_params);
        if (result.Item) {
          // redirect to back to the URL
          redirect_url = result.Item.redirect_url + "?error=" + err.message;
        } else {
          return failure({
            status: false,
            error: "API accesses for the supplied auth_token not found."
          });
        }
      } catch (e) {
        return failure({
          status: false
        });
      }
    }
  );
  return await redirect(redirect_url);
}

var createIntegrationConnection = async (auth_token, integration_item) => {
  const params = {
    TableName: INTEGRATION_CONNECTIONS_TABLE,
    Item: {
      auth_token: auth_token,
      integration_id: integration_item.integration_id,
      integration_connection_status: "inactive",
      key_type: integration_item.key_type,
      key_value: integration_item.key_value
    }
  };

  try {
    await dynamoDbLib.call("put", params);
    console.log("integration connections created for :: " + auth_token + ":" + integration_item.integration_id);
    return params.Item;
  } catch (e) {
    console.log("Error :: " + e);
    return failure({
      status: false
    });
  }
};

var getDTRBearerToken = async (integrationConnectionItem, code, resolve, reject) => {
  // let url = "https://account-d.docusign.com/oauth/token";
  let url = integrationConnectionItem.key_value.dtr_bearer_token_url;
  let key = integrationConnectionItem.key_value.integrator_key + ":" + integrationConnectionItem.key_value.secret_key;
  let headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": "Basic " + Buffer.from(key).toString("base64")
  };
  let params = {
    "grant_type": "authorization_code",
    "code": code
  };

  await axios({
      method: "POST",
      url: url,
      headers: headers,
      params: params
    })
    .then(async response => {
      console.log("\n bearer token :: " + response.data.access_token);
      if (response.data.access_token != null && response.data.access_token.length > 0) {
        let bearerToken = "Bearer " + response.data.access_token;
         console.log("bearerToken received");
        await getDTRUserInfo(integrationConnectionItem, bearerToken, resolve, reject);
      } else {
        console.log("DTR Bearer Token:Error::");
        return failure({
          status: false,
          error: "Error in get DTR Bearer Token"
        });
      }
    }).catch(err => {
      console.log("caught: DTR Bearer Token:Error::");
      var msg = err.response.data.error;
      console.log(msg);
      if (msg === "invalid_grant") {
        console.log("\n Get fresh grant access code !!!");
      }
      return failure({
        status: false,
        error: err
      });
    });
};


var getDTRUserInfo = async (integrationConnectionItem, token, resolve, reject) => {
  // let url = "https://account-d.docusign.com/oauth/userinfo";
  let url = integrationConnectionItem.key_value.dtr_user_info_url;
  let headers = {
    "Authorization": token
  };
  await axios({
    method: "GET",
    url: url,
    headers: headers
  }).then(async response => {
    console.log("\n USERINFO :: " + JSON.stringify(response.data));
    await verifyDTRAccount(integrationConnectionItem, response.data.sub, response.data.email, resolve, reject).then(data => {
      console.log("returnedResponse ::", data.body.error);
      if (!data.body.status) {
        isError = true;
      }
    });
    
    
  }).catch(err => {
    console.log("DTR User Info:Error::");
    console.log(err);
    return failure({
      status: false,
      error: err
    });
  });
};


var verifyDTRAccount = async (integrationConnectionItem, sub, email, resolve, reject) => {
  // let url = "https://stage.cartavi.com/AccountServer/Account/CheckEmail";
  let url = integrationConnectionItem.key_value.dtr_verify_account_url;
  const params = {
    "email": email
  };
  return await axios({
    method: "GET",
    url: url,
    params: params
  }).then(async response => {
    var accountVerification = JSON.stringify(response.data);
    console.log("\n Account Verification ::" + accountVerification);
    if (response.data.IsAccountServerLink) {
      integrationConnectionItem.key_value.sub = sub;
      integrationConnectionItem.key_type.sub = "string";
      const params = {
        // TODO: Update record in integration connections table
        TableName: INTEGRATION_CONNECTIONS_TABLE,
        Key: {
          auth_token: integrationConnectionItem.auth_token,
          integration_id: integrationConnectionItem.integration_id
        },
        // 'UpdateExpression' defines the attributes to be updated
        // 'ExpressionAttributeValues' defines the value in the update expression
        UpdateExpression: "SET integration_connection_status = :integration_connection_status, key_value = :key_value, key_type = :key_type",
        ExpressionAttributeValues: {
          ":integration_connection_status": "active",
          ":key_value": integrationConnectionItem.key_value,
          ":key_type": integrationConnectionItem.key_type,
        },
        // 'ReturnValues' specifies if and how to return the item's attributes,
        // where UPDATED_NEW returns updated attributes of the item after the update; you
        // can inspect 'result' below to see how it works with different settings
        ReturnValues: "UPDATED_NEW"
      };

      try {
        const result = await dynamoDbLib.call("update", params);
        console.log("\n Integration Connection Active ::" + result.Item);
        resolve("success");
      } catch (e) {
        e.name = "dynamodb_error";
        throw e;
      }
    } else { 
      throw new Error('This user does not have access to Docusign Transaction Rooms');
    }
  }).catch(err => {
    console.log("verify DTR Account:Error::", err);
    reject(err);
  });
};