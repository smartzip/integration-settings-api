var request = require("request");
const yargs = require('yargs');

// Pseudo logic //
// Add the environment variables 
// get auth token by passing API KEY
// Create an integration record

const config = {
    "integration-settings-base-url": {
        "dev": "https://dev.integration-settings-api.smartzip-services.com",
        "staging": "https://stage.integration-settings-api.smartzip-services.com",
        "prod": "https://integration-settings-api.smartzip-services.com"
    },
    "api-key": {
        "dev": "lHcFBFD3EJ21wobrG1t9i4FLhYbJcLmiTGofZiV6",
        "staging": "RENXxtO7QH15VQxb7SegD2ClslRmVx5c99u9jgjW",
        "prod": "UEWeacYnv03eZRnffw0bJhbinbBZS5I4PZCD3iT6"
    },
    "redirect-url": {
        "dev": "https://dev.reach150.com/admin/integrations",
        "staging": "https://staging.reach150.com/admin/integrations",
        "prod": "https://reach150.com/admin/integrations"
    },
    "connect-url": {
        "dev": "https://account-d.docusign.com/oauth/auth?response_type=code&scope=impersonation dtr.rooms.read dtr.company.read&client_id=c7d008c6-a88d-4075-aeea-5f8db0153425&state=temp_state&redirect_uri=https://dev.integration-settings-api.smartzip-services.com/docusign/callback",
        "staging": "https://account.docusign.com/oauth/auth?response_type=code&scope=impersonation dtr.rooms.read dtr.company.read&client_id=c7d008c6-a88d-4075-aeea-5f8db0153425&state=temp_state&redirect_uri=https://stage.integration-settings-api.smartzip-services.com/docusign/callback",
        "prod": "https://account.docusign.com/oauth/auth?response_type=code&scope=impersonation dtr.rooms.read dtr.company.read&client_id=c7d008c6-a88d-4075-aeea-5f8db0153425&state=temp_state&redirect_uri=https://integration-settings-api.smartzip-services.com/docusign/callback"
    },
    "secret-key": {
        "dev": "31cac727-f48f-4ed4-b667-cffcd50ce5ac",
        "staging": "77121b62-7689-4bd6-a534-015afbaf726a",
        "prod": "77121b62-7689-4bd6-a534-015afbaf726a"
    },
    "integrator-key": {
        "dev": "c7d008c6-a88d-4075-aeea-5f8db0153425",
        "staging": "c7d008c6-a88d-4075-aeea-5f8db0153425",
        "prod": "c7d008c6-a88d-4075-aeea-5f8db0153425"
    },
    "private-key": {
        "dev": `-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAnuZolmzzOH2PvNmqUUtGuQfJ/jayOlBLrLxPElJaxDVBZC/p
iTC0FfnOZ8hD/OzQZpx3adlj/dCPNtfkqQbKT8noaTmx8+A8ZbtkPXetGdEK2Wtz
O5rs4smsIHCzP5pxLqox0GJLxiitD1DEzJqLJIMMLieyrH3psW5QtSIxtiol5FPP
yFSVIxzS/VAI44V/LQdnygd1r+p8ZTc6clHLevcNT02125nrFzd+Y0EVl9w0vpXb
166E3UjF1zb3GGami5R7lY+dhtkMs6y38Yrbl/YabUIFWjmua1g37N/ilclu70NM
Tw8CYqYzpRBI2XalJtROPDvZDlEnBqpsztvM0wIDAQABAoIBAAkzBAK9cQFSvPPC
fZVHmzhoG94fUtyEZzUrZmqES7lImgNlBgtev0uhvZI4P74QrxSvDKIbUf97nRGg
Qx6JWZoanDLHV9GWKE5Syyqx1zAuRYdb11KFQB9LF1ooxtUw/vTO0/khT7YRBx/i
swS4vRh9Nm3PojemnH/HXV3Min3mGxKUs1G4aTAeyoSlq2JYMJgV/9CaMjhkY2G1
DYM2Y6Kz/2bnsDlXtqmVdg/nLLnLCTKDJbpSEPAcScMWMoXgHdWIgSSnQ7h64uVf
JMdEC53jqtFHYfI7GvQ3jwo7f4oAnQwqvmWnGH3bus4AorkWR7C80IJxej8whLT7
pmr078UCgYEA0VLeO/PWutOOSNZDmVEzJ3m4SHYUSD+z4/Qwakz62LK7gCuWCNAC
zYZXqigp2O2kpusiB8smx1H804vY64d9srGvxOx+XEBJDqlfkc9civA+qYUyK8qB
5FhQCObWh1kTEB1+T/d6+0FYNyCLiCNRTuhFmsjqt1vJNpWvUfw8RVUCgYEAwlUh
1DcE2RJZkJbKws7/GPdzEbP2OcedrJkTXtqNtk9NdULQ4/oEn0iDdhUzVJMc2NRA
BwDzQDbXF/PE2pZ0rEO2Lf8ZtW9GTH4l6kT8Yejue/bbx69WGZRf+yyYHry67sCC
vxLB3YaFKuCzWXbeP2dmb/BGo14G5Q+bHw4WSYcCgYA76NDMLeTGQehWNmTQi6nt
YWv56Tmy1/CfjHMS6Ydd4AHEFDVzvj0fH5UVkTsw6AsY7vElc+JcZ73JuhvShUvX
if15igo8UvXV6Wn97HBbH5l/oSyT1fjJJvF5KsA5qqfHE/nj3zvmj3Q1Cu6Efz81
K0tImrp60h1LrxmniF6jpQKBgF9gcwL5odp8BBNPvuIPv+ncANDOd2lyTDOayYQD
VgwFLeoASg+p9LzMVSEOVKwMq9rLiUUmtTxt7fFmhdYWg9rMPMykaFTA8Uq3mxo4
k3nmXa3fwYxmxKY3SUg4YcysKj7lowXuSdHUpOUfdUbSbU/HCuKjlbe2fUHNAAAk
5NFfAoGAWnVQOCXeybG6ULAoWrrP3kZ4BDaLXfVA2BBR6yM21o7S1s57eYwkZFZ3
Qy8Xn0fAIMiV/kt+U3Kpm4tbLPK307Un4vhDO6xsvfld9Jk7OPL/t2bPRpDnvkEd
X4GPK7U8sZXdZ7ML6Eqw0l9IRXmDJFRZqRK2EWF+VX83U+zuVBM=
-----END RSA PRIVATE KEY-----`,
        "staging": `-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAuz6rdEfoedfA3r6UOkbzXC8xbolbcDt3yjxudW+6XKU+7b+K
i8gnVrnHuZyoFk28DmJjIBzi8RIESJTJYpP7HgsM5JQBicH2Lp1zvImETO5U6oF7
zCg741l1B7KUYLnoS+H+lGEnAyMoPSqhV84IHx4rRHb5V1ooJUZUUauEld1LBXk8
z4X3bcSJAGwGYq8zP6ebldfBAdNujmUwhZD8XzmEKhD0Qw7IiTZaa96KknpmzG2M
GLi6Mtmxru+YrG2GCW0OUXUNMLQfGkBbXqAlKxwzklWwY+T2q2nwpMKcKBlA46qb
Lfb/Cj5ISQqgOZ0n2m250PmI5LoOzAmf+mA6GwIDAQABAoIBADykn/ABqAvqMieS
DrzPJNta14Ymy2wOfo4Lw3Sacn+17yseCXebJA6FDegSEihjFcCM4fDRGjkcbjG5
qn2YUtDYoFzZiKP+o0ZWrVjHKwelevAWbqtg7oCEBRCwqDipjVAiSgrxaiqsdMhW
qbmf22aEpVu/sqK1auIjpIRs6fyFCCD2/yXYK6fecVi1UabYHfDX4W4vuapHK15J
bPJP/lXWEhpo4OuLp8TLUiVW9+eb0kpkca59V5sNUW1QQknMFzUdDohTxlkQ0qDW
uWbWVWA4nU8EWP7wr+jGXff3DX1D0i6DVPs/NGuXhmRS2V1g9Kdvv/prC+0sbN6L
0sa5LJUCgYEA9SOBGGbu1qBpJjdoSaMwivZi2LNGuu21VX8GXpGBVEAG2TpQDVIX
DSzrmqM+cP9qFWhdx57Dfl7JkNBsjHvvy8+9w8qLG3tcx2iErAa+FXtKrzmRn6xv
QucnLnXoFtJk+g1yeBPQoMuTmaUrF0NJtFbggbjY9M2rs0kvQHNmOGUCgYEAw4qA
bgS+73K/b1zsF+1MjwfxSkGox5Qg7Kbv3lZMZmXPWjmMey76bgOf7jE9pjp/Un3q
30IxnBgfusDhxGVWc2tCFVbs84aEucKQ5VC77Y9EGIUnhRVYnOxzWvF0/gA6/gbZ
U3EI6croABVn+notuWDY7UQYX3QPWhs8JcK9QH8CgYEApqzNSdoy6puG4wUpU2gx
yUpdSxoaLV8SvF7x5brpRG+EOI79fxEhu0OfrjTwb8Q6TPP5T5ETOwkX4+7S33Il
SV6mmElO7BCktLDMBGtRMynxTdLGwNpti/+/FEf21g2Xuq0J97sBT1MbuIwlEFwf
8Q5g99LHMSvmYrCQADJ2fkECgYBIJfFPJrcgeHFD8MA379mgqKiR4noY8YHYb5vv
qF3WJliEjNlVljbWosW+yDJcBBd4Y3dm2vCdDJWgfYyPv2hPe9LNNt6E2Cxesfhu
dARzGbphZbf4NbEVbXT+qDqngYRCuFo1nG5SfQhvB3bvB2eZj69alBZ+Gb+UWpot
wJW9lwKBgH9xAm1Xjbk1+PYwlL09X6YGpPq9o275euQZAWJehizOSWTHqTXvy74S
lDyBe5zXPqqTvNLGtAEXTFh9FvnB+yN0UqIAHj1fxr0cVV+zyK6atrapCCDGu8Zu
GD7W3KIU86Vrc2JuJ+CBY7k7GAEVSjlThIAA1Si1uj8K5yLr2mKi
-----END RSA PRIVATE KEY-----`,
        "prod": `-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAuz6rdEfoedfA3r6UOkbzXC8xbolbcDt3yjxudW+6XKU+7b+K
i8gnVrnHuZyoFk28DmJjIBzi8RIESJTJYpP7HgsM5JQBicH2Lp1zvImETO5U6oF7
zCg741l1B7KUYLnoS+H+lGEnAyMoPSqhV84IHx4rRHb5V1ooJUZUUauEld1LBXk8
z4X3bcSJAGwGYq8zP6ebldfBAdNujmUwhZD8XzmEKhD0Qw7IiTZaa96KknpmzG2M
GLi6Mtmxru+YrG2GCW0OUXUNMLQfGkBbXqAlKxwzklWwY+T2q2nwpMKcKBlA46qb
Lfb/Cj5ISQqgOZ0n2m250PmI5LoOzAmf+mA6GwIDAQABAoIBADykn/ABqAvqMieS
DrzPJNta14Ymy2wOfo4Lw3Sacn+17yseCXebJA6FDegSEihjFcCM4fDRGjkcbjG5
qn2YUtDYoFzZiKP+o0ZWrVjHKwelevAWbqtg7oCEBRCwqDipjVAiSgrxaiqsdMhW
qbmf22aEpVu/sqK1auIjpIRs6fyFCCD2/yXYK6fecVi1UabYHfDX4W4vuapHK15J
bPJP/lXWEhpo4OuLp8TLUiVW9+eb0kpkca59V5sNUW1QQknMFzUdDohTxlkQ0qDW
uWbWVWA4nU8EWP7wr+jGXff3DX1D0i6DVPs/NGuXhmRS2V1g9Kdvv/prC+0sbN6L
0sa5LJUCgYEA9SOBGGbu1qBpJjdoSaMwivZi2LNGuu21VX8GXpGBVEAG2TpQDVIX
DSzrmqM+cP9qFWhdx57Dfl7JkNBsjHvvy8+9w8qLG3tcx2iErAa+FXtKrzmRn6xv
QucnLnXoFtJk+g1yeBPQoMuTmaUrF0NJtFbggbjY9M2rs0kvQHNmOGUCgYEAw4qA
bgS+73K/b1zsF+1MjwfxSkGox5Qg7Kbv3lZMZmXPWjmMey76bgOf7jE9pjp/Un3q
30IxnBgfusDhxGVWc2tCFVbs84aEucKQ5VC77Y9EGIUnhRVYnOxzWvF0/gA6/gbZ
U3EI6croABVn+notuWDY7UQYX3QPWhs8JcK9QH8CgYEApqzNSdoy6puG4wUpU2gx
yUpdSxoaLV8SvF7x5brpRG+EOI79fxEhu0OfrjTwb8Q6TPP5T5ETOwkX4+7S33Il
SV6mmElO7BCktLDMBGtRMynxTdLGwNpti/+/FEf21g2Xuq0J97sBT1MbuIwlEFwf
8Q5g99LHMSvmYrCQADJ2fkECgYBIJfFPJrcgeHFD8MA379mgqKiR4noY8YHYb5vv
qF3WJliEjNlVljbWosW+yDJcBBd4Y3dm2vCdDJWgfYyPv2hPe9LNNt6E2Cxesfhu
dARzGbphZbf4NbEVbXT+qDqngYRCuFo1nG5SfQhvB3bvB2eZj69alBZ+Gb+UWpot
wJW9lwKBgH9xAm1Xjbk1+PYwlL09X6YGpPq9o275euQZAWJehizOSWTHqTXvy74S
lDyBe5zXPqqTvNLGtAEXTFh9FvnB+yN0UqIAHj1fxr0cVV+zyK6atrapCCDGu8Zu
GD7W3KIU86Vrc2JuJ+CBY7k7GAEVSjlThIAA1Si1uj8K5yLr2mKi
-----END RSA PRIVATE KEY-----`
    },
    "public-key": {
        "dev": `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnuZolmzzOH2PvNmqUUtG
uQfJ/ jayOlBLrLxPElJaxDVBZC / piTC0FfnOZ8hD / OzQZpx3adlj / dCPNtfkqQbK
T8noaTmx8+ A8ZbtkPXetGdEK2WtzO5rs4smsIHCzP5pxLqox0GJLxiitD1DEzJqL
JIMMLieyrH3psW5QtSIxtiol5FPPyFSVIxzS / VAI44V / LQdnygd1r + p8ZTc6clHL
evcNT02125nrFzd + Y0EVl9w0vpXb166E3UjF1zb3GGami5R7lY + dhtkMs6y38Yrb
l / YabUIFWjmua1g37N / ilclu70NMTw8CYqYzpRBI2XalJtROPDvZDlEnBqpsztvM
0wIDAQAB
----- END PUBLIC KEY-----`,
        "staging": `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnuZolmzzOH2PvNmqUUtG
uQfJ/ jayOlBLrLxPElJaxDVBZC / piTC0FfnOZ8hD / OzQZpx3adlj / dCPNtfkqQbK
T8noaTmx8+ A8ZbtkPXetGdEK2WtzO5rs4smsIHCzP5pxLqox0GJLxiitD1DEzJqL
JIMMLieyrH3psW5QtSIxtiol5FPPyFSVIxzS / VAI44V / LQdnygd1r + p8ZTc6clHL
evcNT02125nrFzd + Y0EVl9w0vpXb166E3UjF1zb3GGami5R7lY + dhtkMs6y38Yrb
l / YabUIFWjmua1g37N / ilclu70NMTw8CYqYzpRBI2XalJtROPDvZDlEnBqpsztvM
0wIDAQAB
----- END PUBLIC KEY-----`,
        "prod": `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuz6rdEfoedfA3r6UOkbz
XC8xbolbcDt3yjxudW+6XKU+7b+Ki8gnVrnHuZyoFk28DmJjIBzi8RIESJTJYpP7
HgsM5JQBicH2Lp1zvImETO5U6oF7zCg741l1B7KUYLnoS+H+lGEnAyMoPSqhV84I
Hx4rRHb5V1ooJUZUUauEld1LBXk8z4X3bcSJAGwGYq8zP6ebldfBAdNujmUwhZD8
XzmEKhD0Qw7IiTZaa96KknpmzG2MGLi6Mtmxru+YrG2GCW0OUXUNMLQfGkBbXqAl
KxwzklWwY+T2q2nwpMKcKBlA46qbLfb/Cj5ISQqgOZ0n2m250PmI5LoOzAmf+mA6
GwIDAQAB
-----END PUBLIC KEY-----`
    },
    "dtr-bearer-token-url": {
        "dev": "https://account-d.docusign.com/oauth/token",
        "staging": "https://account.docusign.com/oauth/token",
        "prod": "https://account.docusign.com/oauth/token"
    },
    "dtr-user-info-url": {
        "dev": "https://account-d.docusign.com/oauth/userinfo",
        "staging": "https://account.docusign.com/oauth/userinfo",
        "prod": "https://account.docusign.com/oauth/userinfo"
    },
    "dtr-verify-account-url": {
        "dev": "https://stage.cartavi.com/AccountServer/Account/CheckEmail",
        "staging": "https://realestate.docusign.com/AccountServer/Account/CheckEmail",
        "prod": "https://realestate.docusign.com/AccountServer/Account/CheckEmail"
    },

};

// CLI configuration
const argv = yargs
    .options({
        stage: {
            demand: true,
            alias: 's',
            describe: 'enter stage name: [dev, staging, prod]',
            string: true
        }
    })
    .help()
    .alias('help', 'h').argv;


async function run(stage) {

    var options = {
        method: 'POST',
        url: config["integration-settings-base-url"][stage] + '/register',
        headers:
        {
            'cache-control': 'no-cache',
            'Content-Type': 'application/json',
            'x-api-key': config["api-key"][stage]
        },
        body:
        {
            app_name: 'reach150_st5'
        },
        json: true
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        console.log(body);

        var options = {
            method: 'POST',
            url: config["integration-settings-base-url"][stage] + '/integrations',
            headers:
            {
                'cache-control': 'no-cache',
                'Content-Type': 'application/json',
                Authorization: body.auth_token
            },
            body:
            {
                integration_status: 'active',
                integration_name: 'Docusign Transaction Rooms',
                integration_short_name: 'dtr',
                description: 'DocuSign Transaction Rooms provides a complete and secure way for real estate professionals to manage their transactions.  Connect your DocuSign Transaction Rooms account to automatically request recommendations from your clients at the time a transaction closes.',
                key_type:
                {
                    integrator_key: 'string',
                    secret_key: 'string',
                    private_key: 'string',
                    public_key: 'string',
                    connect_url: 'string',
                    dtr_bearer_token_url: 'string',
                    dtr_user_info_url: 'string',
                    dtr_verify_account_url: 'string'
                },
                access_level: 'organization',
                key_value:
                {
                    integrator_key: config["integrator-key"][stage],
                    secret_key: config["secret-key"][stage],
                    private_key: config["private-key"][stage],
                    public_key: config["public-key"][stage],
                    connect_url: config["connect-url"][stage],
                    dtr_bearer_token_url: config["dtr-bearer-token-url"][stage],
                    dtr_user_info_url: config["dtr-user-info-url"][stage],
                    dtr_verify_account_url: config["dtr-verify-account-url"][stage]
                }
            },
            json: true
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);

            console.log(body);
            console.log("Seeding success");
        });



    });

}

run(argv.stage);
// Run the script
// $ node seed_docusign.js -s prod