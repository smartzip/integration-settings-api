export function success(body) {
  return buildResponse(200, body);
}

export function success_html(body) {
  return buildHTMLResponse(200, body);
}

export function redirect(location) {
  return {
    statusCode: 301,
    headers: {
      Location: location
    }
  };
}

export function failure(body) {
  return buildResponse(500, body);
}

function buildResponse(statusCode, body) {
  return {
    statusCode: statusCode,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    },
    body: JSON.stringify(body)
  };
}

function buildHTMLResponse(statusCode, body) {
  return {
    statusCode: statusCode,
    headers: {
      'Content-Type': 'text/html',
    },
    body: body
  };
}