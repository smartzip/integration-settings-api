import uuid from "uuid";
import * as dynamoDbLib from "./libs/dynamodb-lib";
import { success, success_html, failure } from "./libs/response-lib";
const INTEGRATIONS_TABLE = `${process.env.DYNAMODB_TABLE_PREFIX}-integrationsTable`;
const API_ACCESSES_TABLE = `${process.env.DYNAMODB_TABLE_PREFIX}-apiAccessesTable`;

export async function index(event, context) {
  const params = {
    TableName: INTEGRATIONS_TABLE
  };

  try {
    const result = await dynamoDbLib.call("scan", params);
    // Return the matching list of items in response body
    return success(result.Items);
  } catch (e) {
    console.log(e);
    return failure({ status: false });
  }
  
}

export async function show(event, context) {
  const params = {
    TableName: INTEGRATIONS_TABLE,
    Key: {
      integration_id: event.pathParameters.id
    },
  };
  
  try {
    const result = await dynamoDbLib.call("get", params);
    console.log("--result: ", result);
    if (result.Item) {
      // Return the retrieved item
      return success(result.Item);
    } else {
      return failure({ status: false, error: "Item not found." });
    }
  } catch (e) {
    return failure({ status: false });
  }
}

export async function update(event, context) {
  const data = JSON.parse(event.body);

  const params = {
    TableName: INTEGRATIONS_TABLE,
    // 'Key' defines the partition key and sort key of the item to be updated
    Key: {
      integration_id: event.pathParameters.id
    },
    // 'UpdateExpression' defines the attributes to be updated
    // 'ExpressionAttributeValues' defines the value in the update expression
    UpdateExpression: "SET access_level = :access_level, description = :description, integration_name = :integration_name, integration_status = :integration_status, key_type = :key_type, key_value = :key_value, integration_short_name = :integration_short_name",
    ExpressionAttributeValues: {
      ":access_level": data.access_level,
      ":description": data.description,
      ":integration_status": data.integration_status,
      ":integration_name": data.integration_name,
      ":integration_short_name": data.integration_short_name,
      ":key_type": data.key_type,
      ":key_value": data.key_value,
    },
    // 'ReturnValues' specifies if and how to return the item's attributes,
    // where ALL_NEW returns all attributes of the item after the update; you
    // can inspect 'result' below to see how it works with different settings
    ReturnValues: "ALL_NEW"
  };

  try {
    const result = await dynamoDbLib.call("update", params);
    return success({ status: true });
  } catch (e) {
    return failure({ status: false });
  }
}

export async function create(event, context) {
  try {
    console.log("--event.body: ", event.body);
    const data = JSON.parse(event.body);
    console.log("--data: ", data);
    const params = {
      TableName: INTEGRATIONS_TABLE,
      Item: {
        integration_id: uuid.v1(),
        integration_status: data.integration_status,
        integration_name: data.integration_name,
        integration_short_name: data.integration_short_name,
        description: data.description,
        key_type: data.key_type,
        access_level: data.access_level,
        key_value: data.key_value
      }
    };


    await dynamoDbLib.call("put", params);
    return success(params.Item);
  } catch (e) {
    console.log("error :: " + e);
    return failure({ status: false });
  }
}

export async function authentication(event, context) {
  const auth_token = event.pathParameters.id.split(':')[0];
  const integration_id = event.pathParameters.id.split(':')[1];

  var redirect_url = "";

  const api_accesses_params = {
    TableName: API_ACCESSES_TABLE,
    Key: {
      auth_token: auth_token
    },
  };
  console.log("api_accesses :: success :: auth_token ::" + auth_token);

  try {
    const result = await dynamoDbLib.call("get", api_accesses_params);
    console.log("api_accesses :: result " + JSON.stringify(result));
    if (result.Item) {
      // redirect to back to the URL
      redirect_url = result.Item.redirect_url;
      console.log("api_accesses ::" + redirect_url);
    } else {
      console.log("api_accesses :: API accesses for the supplied auth_token not found");
      return failure({
        status: false,
        error: "API accesses for the supplied auth_token not found."
      });
    }
  } catch (e) {
    console.log("api_accesses :: catch exception ::" + e);
    return failure({
      status: false
    });
  }

  const params = {
    TableName: INTEGRATIONS_TABLE,
    Key: {
      integration_id: integration_id
    },
  };

  try {
    const result = await dynamoDbLib.call("get", params);
    console.log("--result: ", result);
    if (result.Item) {
      // Return the retrieved item

      let dynamicHtml = '';

      // check for GET params and use if available
      let auth_form_fields = result.Item.key_value.auth_form_fields;
      if (auth_form_fields) {
        dynamicHtml = `<form action="/integrationConnections/${auth_token}:${integration_id}/callbacks" method="post">
                                <div class='form-authentication'>`;

        for (let i = 0; i < auth_form_fields.length; i++) {
          dynamicHtml += `<div class="form-row">
                              <div class="form-group col-md-offset-3 col-md-6">
                                  <label for="input_${auth_form_fields[i]}">${auth_form_fields[i]}</label>
                                  <input type="text" class="form-control auth_form_fields" 
                                          id="input_${auth_form_fields[i]}" placeholder="${auth_form_fields[i]}" 
                                          name="${auth_form_fields[i]}">
                              </div>
                          </div>`;
        }
        // dynamicHtml += `<input type="hidden" class="auth_form_fields" name="integration_id" value="${integration_id}" >`;
        dynamicHtml += "</div>";
        dynamicHtml += `<div class="form-row">
                            <div class="form-group col-md-offset-3 col-md-6">
                                <input type="submit" class="btn btn-warning">
                                <a href="javascript:goBack('${redirect_url}'); " class="btn btn-default">Cancel</a>
                            </div>
                        </div>
                        </form>`;
      }

      const html = `
            <html>
                <meta name="viewport" content="width=device-width, initial-scale=1">

                <!-- Latest compiled and minified CSS -->
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

                <!-- jQuery library -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

                <!-- Latest compiled JavaScript -->
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
                <style>
                h1 { color: #73757d; }
                </style>
                <body>
                    <div class="wrapper">
                        <div class="container">
                            <div class="form-row">
                                <div class="form-group col-md-offset-3 col-md-6">
                                    <h1>Authentication Form</h1>
                                </div>
                            </div>
                            ${dynamicHtml}
                        </div>
                    </div>
                </body>
            </html>

            <script type='text/javascript'>
              function goBack(url) {
                window.location = url; //specify the url to redirect
              }

              /* attach a submit handler to the form */
              $("form").submit(function(event) {

                /* stop form from submitting normally */
                event.preventDefault();

                /* get the action attribute from the <form action=""> element */
                var $form = $( this );
                var url = $form.attr( 'action' );
                var data = JSON.stringify($form.serializeArray());

                /* Send the data using post with element id name and name2*/
                var posting = $.ajax({
                  type: "post",
                  url: url,
                  data: data,
                  headers: {
                    Authorization: '${auth_token}'
                  },
                  contentType: "application/x-www-form-urlencoded",
                  success: function (responseData, textStatus, jqXHR) {
                    console.log(responseData.redirect_url);
                    console.log(responseData);
                    window.location = responseData.redirect_url;
                  },
                  error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                  }
                })

                /* Alerts the results */
                posting.done(function( data ) {
                  // alert("data saved");
                });
              });
          </script>   
            `;
      return success_html(html);
    } else {
      return failure({
        status: false,
        error: "Item not found."
      });
    }
  } catch (e) {
    console.log(e);
    return failure({
      status: false
    });
  }
}