import * as dynamoDbLib from "./libs/dynamodb-lib";
import {success, success_html, failure, redirect} from "./libs/response-lib";
var merge = require('lodash.merge');

const API_ACCESSES_TABLE = `${process.env.DYNAMODB_TABLE_PREFIX}-apiAccessesTable`;
const INTEGRATIONS_TABLE = `${process.env.DYNAMODB_TABLE_PREFIX}-integrationsTable`;
const INTEGRATION_CONNECTIONS_TABLE = `${process.env.DYNAMODB_TABLE_PREFIX}-integrationConnectionsTable`;

export async function index(event, context) {
    try {
        let integration_id;
        if (event.queryStringParameters) {
            integration_id = event.queryStringParameters.integration_id;
        }
        let params;
        if (integration_id) {
            params = {
                TableName: INTEGRATION_CONNECTIONS_TABLE,
                IndexName: 'integrationConnectionsIndexByIntegrationId',
                KeyConditionExpression: 'integration_id = :integration_id',
                FilterExpression: 'integration_id = :integration_id',
                ExpressionAttributeValues: {
                    ':integration_id': integration_id
                },
            };
        } else {
            params = {
                TableName: INTEGRATION_CONNECTIONS_TABLE,
                IndexName: 'integrationConnectionsIndexByAuthToken',
                KeyConditionExpression: 'auth_token = :auth_token',
                FilterExpression: 'auth_token = :auth_token',
                ExpressionAttributeValues: {
                    ':auth_token': event.headers.Authorization
                },
            };
        }

        const result = await dynamoDbLib.call("scan", params);
        // Return the matching list of items in response body
        console.log("integration connections list count :: " + result.Items.length);
        return success(result.Items);
    } catch (e) {
        console.log("Error :: " + e);
        return failure({
            status: false
        });
    }

}

export async function create(event, context) {
    const data = JSON.parse(event.body);
    const params = {
        TableName: INTEGRATION_CONNECTIONS_TABLE,
        Item: {
            auth_token: data.auth_token,
            integration_id: data.integration_id,
            integration_connection_status: data.status,
            key_type: data.key_type,
            key_value: data.key_value
        }
    };

    try {
        await dynamoDbLib.call("put", params);
        console.log("integration connections created for :: " + data.auth_token + ":" + data.integration_id);
        return success(params.Item);
    } catch (e) {
        console.log("Error :: " + e);
        return failure({
            status: false
        });
    }

}

export async function get(event, context) {
    const auth_token = event.pathParameters.id.split(':')[0];
    const integration_id = event.pathParameters.id.split(':')[1];

    const params = {
        TableName: INTEGRATION_CONNECTIONS_TABLE,
        Key: {
            auth_token: auth_token,
            integration_id: integration_id
        },
    };

    try {
        const result = await dynamoDbLib.call("get", params);
        if (result.Item) {
            // Return the retrieved item
            console.log("integration connections get :: " + auth_token + ":" + integration_id);
            return success(result.Item);
        } else {
            console.log("Error :: integration connections Get :: " + auth_token + ":" + integration_id);
            return failure({
                status: false,
                error: "Item not found."
            });
        }
    } catch (e) {
        return failure({
            status: false
        });
    }
}

export async function update(event, context) {
    const data = JSON.parse(event.body);

    const auth_token = event.pathParameters.id.split(':')[0];
    const integration_id = event.pathParameters.id.split(':')[1];

    const params = {
        TableName: INTEGRATION_CONNECTIONS_TABLE,
        // 'Key' defines the partition key and sort key of the item to be updated
        Key: {
            auth_token: auth_token,
            integration_id: integration_id
        },
        // 'UpdateExpression' defines the attributes to be updated
        // 'ExpressionAttributeValues' defines the value in the update expression
        UpdateExpression: "SET key_type = :key_type, key_value = :key_value, integration_connection_status = :integration_connection_status",
        ExpressionAttributeValues: {
            ":key_type": data.key_type,
            ":key_value": data.key_value,
            ":integration_connection_status": data.integration_connection_status
        },
        // 'ReturnValues' specifies if and how to return the item's attributes,
        // where ALL_NEW returns all attributes of the item after the update; you
        // can inspect 'result' below to see how it works with different settings
        ReturnValues: "ALL_NEW"
    };

    try {
        await dynamoDbLib.call("update", params);
        console.log("integration connections update :: " + auth_token + ":" + integration_id);
        return success({
            status: true
        });
    } catch (e) {
        console.log("Error :: " + e);
        return failure({
            status: false
        });
    }
}

export async function update_status(event, context) {
    const auth_token = event.pathParameters.id.split(':')[0];
    const integration_id = event.pathParameters.id.split(':')[1];
    const status = event.queryStringParameters.status;
    // var redirect_url = null;
    // try {
    //     redirect_url = event.queryStringParameters.redirect_url;
    // } catch (error) {
    //     console.log("API Request not containing redirect_url");
    // }

    const params = {
        TableName: INTEGRATION_CONNECTIONS_TABLE,
        Key: {
            auth_token: auth_token,
            integration_id: integration_id
        },
        UpdateExpression: "SET integration_connection_status = :integration_connection_status",
        ExpressionAttributeValues: {
            ":integration_connection_status": status
        },
        ReturnValues: "UPDATED_NEW"
    };

    try {
        await dynamoDbLib.call("update", params);
        console.log("integration connections update status :: " + auth_token + ":" + integration_id);
        // if (redirect_url !== null) {
        //     return redirect(redirect_url);
        // } else {
        //     return success({ status: true });
        // }
        return success({
            status: true
        });
    } catch (e) {
        console.log("Error :: " + e);
        return failure({
            status: false
        });
    }
}

export async function callbacks(event, context) {
    const auth_token = event.pathParameters.id.split(':')[0];
    const integration_id = event.pathParameters.id.split(':')[1];
  
    var redirect_url = "";

    await new Promise(async function (resolve, reject) {

        try {
            const body = JSON.parse(event.body);

            let key_type = {};
            let key_value = {};
            body.forEach(data => {
                key_type[data.name] = "string";
                key_value[data.name] = data.value;
            });

            var int_conn = await createAndUpdateIntegrationConnection(auth_token, integration_id, key_type, key_value);
            resolve("success");
        } catch (e) {
            reject(e);
        }
    }).then(
    async function (success) {
        const api_accesses_params = {
            TableName: API_ACCESSES_TABLE,
            Key: {
            auth_token: auth_token
            },
        };
        console.log("api_accesses :: success :: auth_token ::" + auth_token);
        
        try {
          const result = await dynamoDbLib.call("get", api_accesses_params);
          console.log("api_accesses :: result " + JSON.stringify(result));
          if (result.Item) {
            // redirect to back to the URL
            redirect_url = result.Item.redirect_url;
            console.log("api_accesses ::" + redirect_url);
          } else {
            console.log("api_accesses :: API accesses for the supplied auth_token not found");
            return failure({
              status: false,
              error: "API accesses for the supplied auth_token not found."
            });
          }
      } catch (e) {
        console.log("api_accesses :: catch exception ::" + e);
        return failure({
          status: false
        });
      }
    },
    async function (err) {
      const api_accesses_params = {
        TableName: API_ACCESSES_TABLE,
        Key: {
          auth_token: auth_token
        },
      };
        console.log("api_accesses :: error");

      try {
        const result = await dynamoDbLib.call("get", api_accesses_params);
        if (result.Item) {
          // redirect to back to the URL
          redirect_url = result.Item.redirect_url + "?error=" + err.message;
          console.log("api_accesses ::" + redirect_url);
        } else {
          console.log("api_accesses :: API accesses for the supplied auth_token not found");
          return failure({
            status: false,
            error: "API accesses for the supplied auth_token not found."
          });
        }
      } catch (e) {
        console.log("api_accesses :: catch exception ::" + e);
        return failure({
          status: false
        });
      }
    }
  );
  return await success({'redirect_url': redirect_url});
}

var createAndUpdateIntegrationConnection = async (auth_token, integration_id, key_type, key_value) => {

        const int_params = {
            TableName: INTEGRATIONS_TABLE,
            Key: {
                integration_id: integration_id
            },
        };

            const result = await dynamoDbLib.call("get", int_params);
            var integration_item = result.Item;
            if (integration_item) {

                const int_conn_params = {
                    TableName: INTEGRATION_CONNECTIONS_TABLE,
                    Item: {
                        auth_token: auth_token,
                        integration_id: integration_item.integration_id,
                        integration_connection_status: "inactive",
                        key_type: merge(integration_item.key_type, key_type),
                        key_value: merge(integration_item.key_value, key_value)
                    }
                };

                    await dynamoDbLib.call("put", int_conn_params);
                    console.log("integration connections created for :: " + auth_token + ":" + integration_item.integration_id);
                    return int_conn_params.Item;

            } else {
                throw "integration not found";
            }

};