'use strict';

const jwt = require('jsonwebtoken');

const SECRET_KEY = 'vXrZMXyD9AQQY7xjZgB6uaPpl6EjCX9N';

module.exports.generatePolicy = (token, methodArn) => {
    if (decodeToken(token) !=  null) {
        // token was decoded successfully
        console.log('token was decoded successfully');
        return generateIAMPolicy('user', 'Allow', '*');
        // return generateIAMPolicy('user', 'Allow', methodArn);
    } else {
        console.log('token decode was unsuccessful');
        const error = new Error('Unauthorized');
        throw error;
    }
};

module.exports.generateToken = jsonToSign => {
    var token = jwt.sign(jsonToSign, SECRET_KEY);
    console.log(token);
    return token;
};

var decodeToken = token => {
    try {
        var decoded = jwt.verify(token, SECRET_KEY);
        console.log(decoded);
        return decoded;
    } catch (error) {
        console.log(error);
        return null;
    }
};

// Help function to generate an IAM policy
var generateIAMPolicy = function(principalId, effect, resource) {
    var authResponse = {};
    
    authResponse.principalId = principalId;
    if (effect && resource) {
        var policyDocument = {};
        policyDocument.Version = '2012-10-17'; 
        policyDocument.Statement = [];
        var statementOne = {};
        var statementTwo = {};
        statementOne.Action = 'execute-api:Invoke'; 
        statementOne.Effect = effect;
        statementOne.Resource = resource;
        statementTwo.Action = 'dynamodb:*';
        statementTwo.Effect = 'Allow';
        statementTwo.Resource = "arn:aws:dynamodb:us-east-1:*:table/*";
        policyDocument.Statement[0] = statementOne;
        policyDocument.Statement[1] = statementTwo;
        authResponse.policyDocument = policyDocument;
    }
    
    // Optional output with custom properties of the String, Number or Boolean type.
    // authResponse.context = {
    //     "stringKey": "stringval",
    //     "numberKey": 123,
    //     "booleanKey": true
    // };
    return authResponse;
};

